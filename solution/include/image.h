#ifndef IMAGE_H
#define IMAGE_H

#include "stdint.h"
#include <malloc.h>

// Описание внутреннего представления картинки struct image, очищенное от деталей формата: высота, ширина, указатель на массив пикселей
struct image {
    uint64_t width, height;
    struct pixel* data;
};

// 1 пиксель: синий, зелёный, красный цвет
struct pixel { uint8_t b, g, r; };

// Функции для работы с изображением

// Получает изображение
struct image get_image(uint32_t width, uint32_t height, struct pixel *data);

// Создаёт изображение
struct image create_image(uint32_t width, uint32_t height);

// Уничтожает изображение
void destroy_image(struct image* image);

#endif
